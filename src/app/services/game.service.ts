import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Jigsaw } from '../module/jigsaw.module';

@Injectable()
export class GameService {
    url = 'http://localhost:4000';
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'            
        })
        };
    constructor(private http: HttpClient) { }

    checkWord(word: Jigsaw) {
        return this.http.post<Jigsaw>(`${this.url}/api/checkword`, word, this.httpOptions);
    }
}