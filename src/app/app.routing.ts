import { Routes, RouterModule } from '@angular/router';

import { JigsawComponent } from './components/jigsaw/jigsaw.component';
import { MainComponent } from './components/main/main.component';
import { NumberComponent } from './components/number/number.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './services/auth.guard';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'jigsaw', component: JigsawComponent, canActivate: [AuthGuard]},
    {path: 'game', component: MainComponent, canActivate: [AuthGuard]},
    {path: 'number', component: NumberComponent, canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);