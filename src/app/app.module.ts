import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { OrderModule } from 'ngx-order-pipe';

import { AppComponent } from './app.component';
import { JigsawComponent } from './components/jigsaw/jigsaw.component';
import { MainComponent } from './components/main/main.component';
import { NumberComponent } from './components/number/number.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';

import { routing } from './app.routing';

import { AlertService } from './services/alert.service';
import { UserService } from './services/user.service';
import { UserAuthenticationService } from './services/user.authentication.service';
import { AuthGuard } from './services/auth.guard';
import { GameService } from './services/game.service';

@NgModule({
  declarations: [
    AppComponent,
    JigsawComponent,
    MainComponent,
    NumberComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    StorageServiceModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    OrderModule
  ],
  providers: [
    AuthGuard,
    AlertService,
    UserAuthenticationService,
    UserService,
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
