import { NgModule } from '@angular/core';

import { UserAuthenticationService } from '../services/user.authentication.service'

@NgModule({
  providers: [UserAuthenticationService],
})
export class User {
  id: number;
  email: string;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
}

