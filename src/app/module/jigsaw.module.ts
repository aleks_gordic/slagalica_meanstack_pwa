import { NgModule } from '@angular/core';

import { UserAuthenticationService } from '../services/user.authentication.service'

@NgModule({
  providers: [UserAuthenticationService],
})
export class Jigsaw {
  answer: string;
}

