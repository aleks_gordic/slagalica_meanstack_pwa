import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthenticationService } from '../../services/user.authentication.service';
import { GameService } from '../../services/game.service';
import { Jigsaw } from '../../module/jigsaw.module';

@Component({
  selector: 'app-jigsaw',
  templateUrl: './jigsaw.component.html',
  styleUrls: ['./jigsaw.component.css']
})
export class JigsawComponent implements OnInit {
  is_loggedin: boolean;
  jigsaw = {
    first: "?",
    second: "?",
    third: "?",
    fourth: "?",
    fifth: "?",
    sixth: "?",
    seventh: "?",
    eighth: "?",
    ninth: "?",
    tenth: "?",
    eleventh: "?",
    twelveth: "?"
  }; // jigsaw tile value
  jigsaw_started: boolean = false;
  is_first: boolean = false; 
  is_second: boolean = false;
  is_third: boolean = false;
  is_fourth: boolean = false;
  is_fifth: boolean = false;
  is_sixth: boolean = false;
  is_seventh: boolean = false;
  is_eighth: boolean = false;
  is_ninth: boolean = false;
  is_tenth: boolean = false;
  is_eleventh: boolean = false;
  is_twelveth: boolean = false;
  jigsaw_answer: string = '';
  jigsaw_timeleft: number = 100;
  interval;

  constructor(private router: Router, private gameService: GameService, private authenticationService: UserAuthenticationService) { }

  ngOnInit() {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
  }

  onStartGame() {
    this.jigsaw_started = true;
    this.RandLetters(12);
    this.jigsaw_countdown();
    this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = this.is_seventh = this.is_eighth = this.is_ninth = this.is_tenth = this.is_eleventh = this.is_twelveth = true;
  }

  // generage random letters
  RandLetters = function (length) {
    var characters = 'ABCČĆDDžĐEFGHIJKLLjMNNjOPRSŠTUVZŽ';
    var charactersLength = characters.length;
    const keys = Object.keys(this.jigsaw);

    for (var k = 0; k < keys.length; k++) {
      var key = keys[k];
      for (var i = 0; i < length; i++) {
        this.jigsaw[key] = characters.charAt(Math.floor(Math.random() * charactersLength));
      }
    }
  }

  jigsaw_countdown() {
    this.interval = setInterval(() => {
      if(this.jigsaw_timeleft > 0) {
        this.jigsaw_timeleft--;
      } else {
        this.jigsaw_timeleft = 100;
        clearInterval(this.interval);
      }
    },1000);
  }

  // letters click events
  onJigsaw1 = function () {
    this.jigsaw_answer += this.jigsaw.first;
    this.is_first = false;
  }
  onJigsaw2 = function () {
    this.jigsaw_answer += this.jigsaw.second;
    this.is_second = false;
  }
  onJigsaw3 = function () {
    this.jigsaw_answer += this.jigsaw.third;
    this.is_third = false;
  }
  onJigsaw4 = function () {
    this.jigsaw_answer += this.jigsaw.fourth;
    this.is_fourth = false;
  }
  onJigsaw5 = function () {
    this.jigsaw_answer += this.jigsaw.fifth;
    this.is_fifth = false;
  }
  onJigsaw6 = function () {
    this.jigsaw_answer += this.jigsaw.sixth;
    this.is_sixth = false;
  }
  onJigsaw7 = function () {
    this.jigsaw_answer += this.jigsaw.seventh;
    this.is_seventh = false;
  }
  onJigsaw8 = function () {
    this.jigsaw_answer += this.jigsaw.eighth;
    this.is_eighth = false;
  }
  onJigsaw9 = function () {
    this.jigsaw_answer += this.jigsaw.ninth;
    this.is_ninth = false;
  }
  onJigsaw10 = function () {
    this.jigsaw_answer += this.jigsaw.tenth;
    this.is_tenth = false;
  }
  onJigsaw11 = function () {
    this.jigsaw_answer += this.jigsaw.eleventh;
    this.is_eleventh = false;
  }
  onJigsaw12 = function () {
    this.jigsaw_answer += this.jigsaw.twelveth;
    this.is_twelveth = false;
  }

  // clear answer
  onDeleteWords = function () {
    this.jigsaw_answer = '';
    this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = this.is_seventh = this.is_eighth = this.is_ninth = this.is_tenth = this.is_eleventh = this.is_twelveth = true;
  }

  // End Jigsaw Game
  onJigsawEnd = function () {
    this.jigsaw_started = false;
    this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = this.is_seventh = this.is_eighth = this.is_ninth = this.is_tenth = this.is_eleventh = this.is_twelveth = false;
    clearInterval(this.interval);
    var send_data: Jigsaw = {
      answer: this.jigsaw_answer
    };
    this.gameService.checkWord(send_data).subscribe((res) => {
      console.log("checkWord result: ", res);
      if (res.success) {
        var message = confirm("Vaša reč je " + this.jigsaw_answer + " i ova reč ne postoji u bazi podataka. Kliknite Ok da pokušate ponovo ili Cancel da nastavite.");
        if (message) {
          this.router.navigate(['/number']);
        } else {
          this.jigsaw_started = true;
          this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = this.is_seventh = this.is_eighth = this.is_ninth = this.is_tenth = this.is_eleventh = this.is_twelveth = true;
          this.jigsaw_answer = '';
          this.jigsaw_timeleft = 100;
          this.RandLetters(12);
          this.jigsaw_countdown();
        }
      } else {
        var message = confirm("Vaša reč je " + this.jigsaw_answer + " i ova reč ne postoji u bazi podataka. Kliknite Ok da pokušate ponovo ili Cancel da nastavite.");
        if (message) {
          this.jigsaw_started = true;
          this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = this.is_seventh = this.is_eighth = this.is_ninth = this.is_tenth = this.is_eleventh = this.is_twelveth = true;
          this.jigsaw_answer = '';
          this.jigsaw_timeleft = 100;
          this.RandLetters(12);
          this.jigsaw_countdown();
        } else {
          this.router.navigate(['/number']);
        }
      }
    },
    (error) => {
      console.log(error);
    });
  }

  logout() {
    this.is_loggedin = false;
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }
}
