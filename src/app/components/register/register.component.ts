import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';
import { User } from '../../module/user.module';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  is_loggedin: boolean;
  user: User;
  registerform: FormGroup;
  loading = false;

  constructor(private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    formBuilder: FormBuilder) { 
      this.registerform = formBuilder.group({
        username: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
      });
    }

  ngOnInit() {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
  }

  register() {
    this.loading = true;
    this.user = this.registerform.value;
    this.userService.create(this.user).subscribe(
      (data) => {
        // set success message and pass true paramater to persist the message after redirecting to the login page
        this.alertService.success('Registration successful', true);
        this.router.navigate(['/login']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }
}
