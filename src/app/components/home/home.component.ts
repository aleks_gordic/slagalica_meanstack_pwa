import { Component, OnInit } from '@angular/core';
import { UserAuthenticationService } from '../../services/user.authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  is_loggedin: boolean;
  
  constructor(private router: Router, private authenticationService: UserAuthenticationService) { }

  ngOnInit() {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
    console.log(this.is_loggedin);
  }

  logout() {
    this.is_loggedin = false;
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }
}
