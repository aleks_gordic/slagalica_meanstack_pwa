import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../../services/game.service';
import { UserAuthenticationService } from '../../services/user.authentication.service';
@Component({
  selector: 'this.-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css']
})
export class NumberComponent implements OnInit {
  is_loggedin: boolean;
  number_started: boolean = false;
  is_first_num: boolean = false;
  is_second_num: boolean = false;
  is_third_num: boolean = false;
  is_fourth_num: boolean = false;
  is_fifth_num: boolean = false;
  is_sixth_num: boolean = false;
  number_answer: string = '';
  number_timeleft: number = 100;
  number = {
    target_number: "???",
    using_number: {
      first: "?",
      second: "?",
      third: "?",
      fourth: "?",
      fifth: "??",
      sixth: "??"
    },
    operation: {
      add: "+",
      minus: "-",
      multiply: "*",
      division: "/",
      bracket_left: "(",
      bracket_right: ")"
    }
  };
  interval;

  constructor(private router: Router, private gameService: GameService, private authenticationService: UserAuthenticationService) { }

  ngOnInit() {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
  }

  onNumberStart = function () {
    this.is_first_num = this.is_second_num = this.is_third_num = this.is_fourth_num = this.is_fifth_num = this.is_sixth_num = true;
    this.RandNumbers();
    this.number_started = true;
    this.number_countdown();
  };

  // generate random numbers
  RandNumbers = function () {
    this.number.target_number = this.randomXToY(100, 999);

    const keys = Object.keys(this.number.using_number);
    var last_values = [25, 50, 75, 100];

    for (var k = 0; k < keys.length; k++) {
      var key = keys[k];
      if (k < 4) {
          this.number.using_number[key] = this.randomXToY(1, 9);
      } else {
          this.number.using_number[key] = last_values[(Math.floor(Math.random() * last_values.length))];
      }
    }
  }

  // generate random number between (x, y)
  randomXToY = function (minVal, maxVal) {
    var randVal = minVal + (Math.random() * (maxVal - minVal));
    return Math.round(randVal);
  }

  number_countdown = function () {
    this.interval = setInterval(() => {
      if(this.number_timeleft > 0) {
        this.number_timeleft--;
      } else {
        this.number_timeleft = 100;
        clearInterval(this.interval);
      }
    },1000);
  };

  onClearFormula = function () {
    this.number_answer = '';
    this.is_first_num = this.is_second_num = this.is_third_num = this.is_fourth_num = this.is_fifth_num = this.is_sixth_num = true;
  }

  // get numbers and operation click events
  onFirstNum = function () {
    this.number_answer += this.number.using_number.first;
    this.is_first_num = false;
  }
  onSecondNum = function () {
    this.number_answer += this.number.using_number.second;
    this.is_second_num = false;
  }
  onThirdNum = function () {
    this.number_answer += this.number.using_number.third;
    this.is_third_num = false;
  }
  onFourthNum = function () {
    this.number_answer += this.number.using_number.fourth;
    this.is_fourth_num = false;
  }
  onFifthNum = function () {
    this.number_answer += this.number.using_number.fifth;
    this.is_fifth_num = false;
  }
  onSixthNum = function () {
    this.number_answer += this.number.using_number.sixth;
    this.is_sixth_num = false;
  }
  onAddNum = function () {
    this.number_answer += this.number.operation.add;
  }
  onMinusNum = function () {
    this.number_answer += this.number.operation.minus;
  }
  onMultiNum = function () {
    this.number_answer += this.number.operation.multiply;
  }
  onDivideNum = function () {
    this.number_answer += this.number.operation.division;
  }
  onLeftBracketNum = function () {
    this.number_answer += this.number.operation.bracket_left;
  }
  onRightBracketNum = function () {
    this.number_answer += this.number.operation.bracket_right;
  }

  onNumberEnd = function () {
    this.number_started = false;
    this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = false;
    clearInterval(this.interval);

    if (eval(this.number_answer) != undefined) {
      var message = confirm("Vaš broj je " + this.number_answer + "= " + eval(this.number_answer) + " Kliknite Ok da pokušate ponovo ili Cancel da nastavite dalje");
      // add action for compare with target number and process
      if (message) {
        this.router.navigate(['/game']);
      } else {
          this.number_started = true;
          this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = true;
          this.number_answer = '';
          this.number_timeleft = 100;
          this.RandNumbers();
          this.number_countdown();
      }
    } else {
      alert("Niste dali odgovor. Molim vas pokušajte ponovo!");
      this.number_started = true;
      this.is_first = this.is_second = this.is_third = this.is_fourth = this.is_fifth = this.is_sixth = true;
      this.number_answer = '';
      this.number_timeleft = 100;
      this.RandNumbers();
      this.number_countdown();
    }
  }

  logout() {
    this.is_loggedin = false;
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }
}
