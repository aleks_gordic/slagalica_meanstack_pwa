import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthenticationService } from '../../services/user.authentication.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  is_loggedin: boolean;
  results = [
    { id: '1', username: 'Nikola', score: 99.9 },
    { id: '2', username: 'Dmitrii', score: 159.2 },
  ];
  constructor(private router: Router, private authenticationService: UserAuthenticationService) { }

  ngOnInit() {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
  }

  createGame() {
    this.router.navigate(['/jigsaw']);
  }

  joinGame() {

  }

  gameDay() {

  }

  logout() {
    this.is_loggedin = false;
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }
}
