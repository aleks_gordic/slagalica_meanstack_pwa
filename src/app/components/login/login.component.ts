import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserAuthenticationService } from '../../services/user.authentication.service';
import { AlertService } from '../../services/alert.service';
import { User} from '../../module/user.module';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  is_loggedin: boolean;
  user: User;
  loginform: FormGroup;
  loading = false;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: UserAuthenticationService,
    private alertService: AlertService, formBuilder: FormBuilder) {
      this.loginform = formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
      });
    }

  ngOnInit() {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
  }

  onLoggedin() {
    this.loading = true;
    this.user = this.loginform.value;
    this.authenticationService.login(this.user.email, this.user.password).subscribe(
      (data) => {
        this.router.navigate(['']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
    });
  }
}
