import { Component } from '@angular/core';
import { UserAuthenticationService } from './services/user.authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  is_loggedin: boolean;

  constructor(private router: Router, private authenticationService: UserAuthenticationService) {
    if(localStorage.getItem('currentUser')) {
      this.is_loggedin = true;
    } else {
      this.is_loggedin = false;
    }
  }

  logout() {
    this.is_loggedin = false;
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }
}
